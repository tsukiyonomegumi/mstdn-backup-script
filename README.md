# Mastodon Backup Script

## About

Mastodonのバックアップを実行するスクリプトです。  
Docker版を想定しています。

## Required


- docker-compose
- [glynnbird/toot](https://github.com/glynnbird/toot)
    - recommend: `npm install -g toot`

## Usage

```shell
git pull ||  git reset --hard origin/master && chmod +x backup.sh
./backup.sh
```

## License

[WTFPL](http://www.wtfpl.net/)

## Original Author

[@tsukiyonomegumi](https://gitlab.com/tsukiyonomegumi)
