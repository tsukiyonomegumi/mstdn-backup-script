#!/bin/bash

SECONDS=0
DB_USER=postgres #.env.productionのデフォルト値
BACKUP_DIR=
TAR_DIR=`date +%Y%m%d_%H%M%S`

workdir=`pwd`
mkdir ${TAR_DIR}

cd ../mastodon

echo "BACK UP START." | toot --visibility unlisted

# postgres
docker-compose exec db pg_dump -U ${DB_USER} > db.sql
mv db.sql ${workdir}/${TAR_DIR}/
db_time=$(date -u -d @${SECONDS} +"%T")

# redis
cp -r ./redis ${workdir}/${TAR_DIR}/
redis_time=$(date -u -d @${SECONDS} +"%T")

# files
mastodondir=`pwd`
ln -s ${mastodondir}/public/system ${workdir}/${TAR_DIR}/system

# exec tar
echo "BACK UP: COMPRESS." | toot --visibility unlisted
cd ${workdir}
tar -hzcvf ./${TAR_DIR}.tar.gz ./${TAR_DIR}
tar_time=$(date -u -d @${SECONDS} +"%T")

# post process
mv -v ./${TAR_DIR}.tar.gz ${BACKUP_DIR}/${TAR_DIR}.tar.gz
# unlinkしたあとにrmしないと本番データが死を迎える
unlink ${workdir}/${TAR_DIR}/system
rm -rf ${workdir}/${TAR_DIR}
tar_time=$(date -u -d @${SECONDS} +"%T")

# toot
echo "BACK UP END. PROCESS TIME: ${tar_time}" | toot --visibility unlisted
